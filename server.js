{
  const parser = require('./parser');

  const processLine = line => {
    let parsedAddress;

    parsedAddress = parser.parseAddress(line);
    console.log(line + ' -> ' + JSON.stringify(parsedAddress));
  }

  const readData = input => {
    let line;
    const totalLines = input.length;

    for (let i = 0; i < totalLines; i++) {
      line = input[i];
      if (line) {
        processLine(line);
      }
    }
    // exit after all data is processed
    process.exit(0);
  }

  /********************Manual stdin**********************/
  const removeLineBreaks = (str) => {
    if (str && str.length >= 2) {
      return str.slice(0, str.length - 2);
    }
    return str;
  }

  process.stdin.resume();
  process.stdin.setEncoding("utf8");

  process.stdin.on('data', (input) => {
    // debug
    // console.log("Data signal : " + input);
    input = removeLineBreaks(input);
    processLine(input);
  });
  /******************************************************/

  module.exports = {
    readData: readData
  }
}
