{

  let sandbox;
  const assert = require('chai').assert,
    expect = require('chai').expect,
    sinon = require('sinon'),
    parser = require('./parser');

  describe('AddressLine Parser Unit Tests ', () => {

    before(() => {
      sandbox = sinon.createSandbox({});
    });

    beforeEach(() => {});

    afterEach(() => {
      sandbox.restore();
    });

    const assertParsedAddress = (inputAddress, expectedStreet, expectedNumber) => {
      const parsedAddress = parser.parseAddress(inputAddress);
      assert.equal(parsedAddress[0], expectedStreet, 'Wrong street value');
      assert.equal(parsedAddress[1], expectedNumber, 'Wrong number value');
    };

    it("should parse 'Winterallee 3' address", () => {
      assertParsedAddress('Winterallee 3', 'Winterallee', '3');
    });
    it("should parse 'Musterstrasse 45' address", () => {
      assertParsedAddress('Musterstrasse 45', 'Musterstrasse', '45');
    });
    it("should parse 'Blaufeldweg ' address", () => {
      assertParsedAddress('Blaufeldweg 123B', 'Blaufeldweg', '123B');
    });
    it("should parse 'Am Bächle 23' address", () => {
      assertParsedAddress('Am Bächle 23', 'Am Bächle', '23');
    });
    it("should parse 'Auf der Vogelwiese 23 b' address", () => {
      assertParsedAddress('Auf der Vogelwiese 23 b', 'Auf der Vogelwiese', '23 b');
    });
    it("should parse '4, rue de la revolution' address", () => {
      assertParsedAddress('4, rue de la revolution', 'rue de la revolution', '4');
    });
    it("should parse '200 Broadway Av' address", () => {
      assertParsedAddress('200 Broadway Av', 'Broadway Av', '200');
    });
    it("should parse 'Calle Aduana, 29' address", () => {
      assertParsedAddress('Calle Aduana, 29', 'Calle Aduana', '29');
    });
    it("should parse 'Calle 39 No 1540' address", () => {
      assertParsedAddress('Calle 39 No 1540', 'Calle 39', 'No 1540');
    });

  });

}
