{
  const parser = {};
  const NUMBER_ADDRESS_REGEXP = /^\d+\w*|(\s+\d+\w*|\s+\d+ +\w){1}$|(No)?\s+\d+\w*$/;
  const COMMA_BEGIN_END_REGEXP = /^\s*,\s*|\s*,\s*$/g;

  const extractNumberAddress = (address) => {
    let result;

    result = address.match(NUMBER_ADDRESS_REGEXP);
    if (result) {
      return result[0];
    }
    return '';
  }

  const removeBeginEndCommas = str => (str || '').replace(COMMA_BEGIN_END_REGEXP, '');

  const extractStreetAddressFromNumberAddress = (address, addressNumber) =>   {
    let parsedStreet = '';
    const numberBeginIndex = address.indexOf(addressNumber);

    if (numberBeginIndex !== -1) {
      if (numberBeginIndex === 0) {
        parsedStreet = address.substring(addressNumber.length);
      } else {
        parsedStreet = address.substring(0, numberBeginIndex);
      }
    }
    parsedStreet = removeBeginEndCommas(parsedStreet);
    return parsedStreet;
  }

  /*
  Returns a parsed address in the following format:
  [street, number]
  */
  parser.parseAddress = (addressStr) => {
    let parsedAddress = [],
      parsedNumber = '',
      parsedStreet = '';

    addressStr = addressStr.trim();
    parsedNumber = extractNumberAddress(addressStr);
    parsedStreet = extractStreetAddressFromNumberAddress(addressStr, parsedNumber);

    parsedAddress.push(parsedStreet.trim());
    parsedAddress.push(parsedNumber.trim());

    return parsedAddress;
  }

  module.exports = parser;
}
